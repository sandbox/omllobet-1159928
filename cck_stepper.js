// $Id$

/**
 * @file
 * Javascript file inserting cck_stepper
 */

Drupal.behaviors.cck_stepper = function(context) {
  var settings = new Array();
  var iter = 0;
  for (var i in Drupal.settings.cck_stepper) {
    settings[iter] = Drupal.settings.cck_stepper[i];
	iter++;
  }
  $(".cck-stepper-field", context).each(function(index) {
    $(this).spinner({
      min: parseInt(settings[index]['min']),
      max: parseInt(settings[index]['max']),
      stepping: parseInt(settings[index]['increment_size']),
      spin: function( event, ui ) {
        $(this).change();
      }
    });
  });
}