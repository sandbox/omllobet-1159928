// $Id$

-- SUMMARY --

This modules adds a stepper widget to cck for integer numbers input

-- REQUIREMENTS --

cck
jquery_ui

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.
* for version 1.6 you should put the ui.spinner.js file in /cck_stepper/ui/
  the right version to work with drupal 6 + jquery_ui 1.6 is in
  http://dl.dropbox.com/u/1041436/stepper.zip
  Warning: it's a beta version not published with jquery_ui 1.6 and patched by me.

-- CONTACT --

Current maintainers:
* Oscar Martinez - http://drupal.org/user/871492
